import { createApp } from 'vue'
import App from './App.vue'
import SvgIcon from "./components/SvgIcon.vue";
import './plugins/common'

createApp(App)
  .component("icon", SvgIcon)
  .mount('#app')
