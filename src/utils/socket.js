
export const connectSocket = (wsUrl,request,callback) => {
  const socket = new WebSocket(wsUrl);
  socket.onopen = function() {
    console.log("websocket connected!!",wsUrl);
    socket.send(JSON.stringify(request));
  };
  socket.onmessage = function(msg) {
    callback(JSON.parse(msg.data).data)
  };
  socket.onerror = function(err) {
    console.log("error", err);
  };
};

export const sendSocketMessage = msg => {
  if (1 === socket.readyState) socket.send(JSON.stringify(msg));
};