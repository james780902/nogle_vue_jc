import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { svgBuilder } from './src/plugins/svgBuilder'; 
export default defineConfig({
  base :'/nogle_vue_jc/',
  plugins: [vue(),svgBuilder('src/assets/icon/')],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
})
